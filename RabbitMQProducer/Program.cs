﻿using ClassLibrary;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace RabbitMQProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );
                TimerCallback tm = new TimerCallback(SendUser);
                Timer timer = new Timer(tm, channel, 0, 1000);

                Console.WriteLine("Press any key to end");
                Console.ReadLine();

            }
        }

        private static void SendUser(object obj)
        {
            var channel = (IModel)obj;
                User user = User.GetRandomUser();
                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, (object)user);
                var body = ms.ToArray();

                
            channel.BasicPublish(exchange: "",
                    routingKey: "homework",
                    basicProperties: null,
                    body: body);
                Console.WriteLine("[x] Send {0}", user.Name);
        }

        private static IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = "rattlesnake.rmq.cloudamqp.com",
                UserName = "lmumpkkt",
                Password = "Vkg0yjrwUmfP5IleSrMhhR-Nq5O9wrrh",
                VirtualHost = "lmumpkkt"
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }
}
