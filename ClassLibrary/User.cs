﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ClassLibrary
{
    [Serializable]
    public class User : ISerializable
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        private static int index;
        private static List<User> randomUser;

        public User()
        {

        }
        public static User GetRandomUser()
        {
            initRandomUser();
            index = index > 0 & index < randomUser.Count ? index : 0;
            return randomUser[index++];
        }
        private static void initRandomUser()
        {

            if (randomUser == null)
            {
                randomUser = new List<User>();
                randomUser.Add(new User() { Name = "Serg", Age = 22, Email = "serg@gmail.com" });
                randomUser.Add(new User() { Name = "Den", Age = 22, Email = "den@gmail.com" });
                randomUser.Add(new User() { Name = "Ivan", Age = 22, Email = null });
            }
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Age", Age);
            info.AddValue("Email", Email);
        }
        public User(SerializationInfo info, StreamingContext context)
        {
            Name = (string)info.GetValue("Name", typeof(string));
            Email = (string)info.GetValue("Email", typeof(string));
            Age = (int)info.GetValue("Age", typeof(int));
        }

        public override string ToString()
        {
            return this.Name + " " + Age + " " + Email;
        }
    };


}
