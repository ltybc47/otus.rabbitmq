﻿using ClassLibrary;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RabbitMQConsumer
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (sender, ea) =>
                {
                    using (var ms = new MemoryStream(ea.Body.ToArray()))
                    {
                        var bf = new BinaryFormatter();
                        User user = (User)bf.Deserialize(ms);
                        Console.WriteLine("[x] Recaived {0}", user.ToString());
                        if (user.Email != null)
                        {

                            channel.BasicAck(ea.DeliveryTag, false);    //  Подтверждение сообщения, после этого оно удаляется с брокера
                        }

                    }
                };
                channel.BasicConsume(queue: "homework",
                    autoAck: false,
                    consumer: consumer);
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = "rattlesnake.rmq.cloudamqp.com",
                UserName = "lmumpkkt",
                Password = "Vkg0yjrwUmfP5IleSrMhhR-Nq5O9wrrh",
                VirtualHost = "lmumpkkt"
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }
}
